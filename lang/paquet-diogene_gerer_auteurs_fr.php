<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_gerer_auteurs.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_gerer_auteurs_description' => 'Permet d’ajouter via Diogene un champ afin d’associer ou dissocier un ou plusieurs auteurs sur un formulaire d’édition d’objet.',
	'diogene_gerer_auteurs_nom' => 'Diogene - Auteurs',
	'diogene_gerer_auteurs_slogan' => 'Complément "auteurs" pour "Diogene"'
);
