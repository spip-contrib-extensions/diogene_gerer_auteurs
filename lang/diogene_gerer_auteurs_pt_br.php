<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_gerer_auteurs?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_cfg_ajout_auteurs' => 'Incluir a possibilidade de incluir / excluir os autores nas matérias criadas (usando o plugin "Diogenes - Autores")',
	'explication_diogene_gerer_auteurs' => 'Para incluir uma outra pessoa como autor, ela deve ser membro do site.',

	// F
	'form_legend' => 'Os autores',

	// L
	'label_cfg_ajout_auteurs' => 'Inclusão/ exclusão de autores',
	'label_diogene_gerer_auteurs' => 'Inclusão e exclusão de autor(es)',

	// M
	'message_impossible_supprimer_auteur' => 'Você não pode se excluir da lista de autores. Você perderia os seus direitos de edição.'
);
