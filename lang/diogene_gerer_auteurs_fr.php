<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_gerer_auteurs.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_cfg_ajout_auteurs' => 'Ajoute la possibilité d’ajouter / supprimer des auteurs sur les articles créés (Utilisera le plugin "Diogène - Auteurs")',
	'explication_diogene_gerer_auteurs' => 'Pour ajouter une autre personne comme auteur, elle doit être membre du site.',

	// F
	'form_legend' => 'Les auteurs',

	// L
	'label_cfg_ajout_auteurs' => 'Ajouts / suppression d’auteurs',
	'label_diogene_gerer_auteurs' => 'Ajout et suppression d’auteur(s)',

	// M
	'message_impossible_supprimer_auteur' => 'Vous ne pouvez vous supprimer vous-même de la liste des auteurs. Vous perdriez vos droits d’édition.'
);
