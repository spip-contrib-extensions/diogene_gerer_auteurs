<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_gerer_auteurs?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_gerer_auteurs_description' => 'Permite añadir a través de Diógenes un campo para asociar o disociar uno o varios autores a un formulario de edición de objeto. ',
	'diogene_gerer_auteurs_nom' => 'Diógenes - Autores',
	'diogene_gerer_auteurs_slogan' => 'Complemento "autores" para "Diógenes"'
);
