<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_gerer_auteurs?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_cfg_ajout_auteurs' => 'Enables to add / remove authors on articles (use the plugin "diogene auteurs")',
	'explication_diogene_gerer_auteurs' => 'To add another person as author, it has to be member of the website.',

	// F
	'form_legend' => 'The authors',

	// L
	'label_cfg_ajout_auteurs' => 'Add / Remove authors',
	'label_diogene_gerer_auteurs' => 'Adding and removing author(s)',

	// M
	'message_impossible_supprimer_auteur' => 'You can’t remove yourself from the list of authors. You will lose your editing rights.'
);
