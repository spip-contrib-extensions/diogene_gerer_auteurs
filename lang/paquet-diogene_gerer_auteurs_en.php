<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_gerer_auteurs?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_gerer_auteurs_description' => 'Enable to add via Diogenes a field to link or unlink one or more authors on a object editing form.',
	'diogene_gerer_auteurs_nom' => 'Diogene - Authors',
	'diogene_gerer_auteurs_slogan' => '"Authors" add-on for "Diogene"'
);
